# OpenML dataset: Dutch-News-Articles

https://www.openml.org/d/43370

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dutch News Articles
This dataset contains all the articles published by the NOS as of the 1st of January 2010. The data is obtained by scraping the NOS website. The NOS is one of the biggest (online) news organizations in the Netherlands.
Features:

datetime: date and time of publication of the article.
title: the title of the news article.
content: the content of the news article.
category: the category under which the NOS filed the article.
url: link to the original article.

About the data
The title and content of features somewhat clean. Meaning extra whites spaces and newlines are removed. Furthermore, these features are normalized (NFKD). The NOS also publishes liveblogs. The posts in this live blog are not part of this dataset. 
Example
I used this dataset in a recent blog post.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43370) of an [OpenML dataset](https://www.openml.org/d/43370). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43370/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43370/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43370/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

